<%@ Page Language="vb" Trace="False" %>

<% If Session("rdLogonReturnURL") = "" Then
        Session("rdLogonReturnURL") = "rdPage.aspx"
    End If %>

<html>
<head>
    <title>Dashboard</title>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <style type="text/css">
			BODY
			{
				font-family: 'Roboto', sans-serif;
				font-size: 14px;				
				margin:50px 0px; padding:0px;
				background-color: #f0f0f0;				
			}	
			
			TABLE
			{
				font-family: 'Roboto', sans-serif;
				font-size: 14px;
                text-align: center;
			}
			
			#rdUserName, #rdPassword
			{
				width: 224px;
                height: 32px;
                border: 1px solid #CCC;
                border-radius: 3px;
                padding: 4px 8px 4px 8px;
                margin-bottom: 4px;
			}
			
			#mainTable
			{				
				margin: 0px;
				padding: 0px;
				width: 100%;
				height: 100%;
			}
			
			#mainCell
			{
				text-align: center;
			}
												
			#logonPanel
			{					
				width: 320px;
				margin: auto;
				background-color: #FFFFFF;
			}
			
			#logonPanel table
			{
				margin: 0px;
				padding: 0px;
				width: 100%;				
			}
						
			#logonHeaderCell
			{
                font-size: 16px;
				margin: 0px;
				padding: 12px 24px 12px 24px;		
				background-color: #FFFFFF;
                text-align: center;
			}

            #logonHeaderSub
			{
				margin: 0px;
				padding: 12px 32px 12px 32px;				
				color: #455a64;
                text-align: center;
			}
			
			.LogonHeader
			{
				font-size: 150%;
				font-weight: bold;
				color: #455a64;				
			}

			#formCell
			{				
				padding: 20px;				
			}
			
			#errorCell, #sessionCell, #errorMisc
			{
				text-align: left;
				padding: 16px 24px 16px 24px;
				color: #790619;				
			}

            .submitLong
            {
                font-size: 12px;
                background-color: #455a64;
                color: White;
                cursor: default;
                border: 1px solid #455a64;
                border-radius: 3px;
                text-transform: uppercase;
                font-family: 'Roboto', sans-serif;
                margin: 8px 0px 8px 0px;
                height: 32px;
                width: 224px;
            }

            #btnSubmit:hover 
            {
                background-color: #375F72;
            }
            
            input:hover 
            {
               background-color: #d7d7d7;   
            }

            .imgHeader
            {
                width: 200px;
                height: 88px;
                padding: 16px 16px 16px 16px;
            } 
		</style>
</head>
<body onkeypress="if (event.keyCode==13){frmLogon.submit()}" onload="document.getElementById('rdUsername').focus()">
    <table id="mainTable" cellspacing="0">
        <tr>
            <td id="mainCell">
                <div id="logonPanel">
                    <table cellspacing="0">
                        <tr>
                            <img id="imgDtap" class="imgHeader" src="dtapLogo.png" alt="DocuTAP Logo" />
                        </tr>
                        <tr>
                            <td id="logonHeaderCell"><span class="LogonHeader">Welcome to Dashboard!</span></td>
                        </tr>
                        <tr>
                            <td id="logonHeaderSub"><span class="LogonHeaderSub">Please log in to access Dashboard. If  you're having issues please contact <a href="http://help.docutap.com/" target="_blank">support.</a></span></td>
                        </tr>
                        <tr>
                            <td id="formCell">
                                <form id="frmLogon" action='<%=Session("rdLogonReturnUrl") %>' method="post">
                                    <table>
                                        <tr>
                                            <td>
                                                <input id="rdUsername" type="text" name="rdUsername" placeholder="Username" required />
                                                <input id="rdFormLogon" type="hidden" name="rdFormLogon" value="True" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input id="rdPassword" type="password" name="rdPassword" placeholder="Password" required />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input id="btnSubmit" class="submitLong" type="submit" value="Log In" name="btnSubmit" />
                                            </td>
                                        </tr>
										<tr>
											<td id="errorMisc" colspan="2">
												<% 	
													Dim lastError AS String = Session("rdLastErrorMessage")
													If String.IsNullOrEmpty(lastError) Then lastError = ""
													
													If Not String.IsNullOrEmpty(Session("rdLogonFailMessage")) Then
														Response.Write(Session("rdLogonFailMessage"))
													ElseIf lastError.indexOf("ADsDSOObject") > -1 Then
														Response.Write("Invalid Username or Password.")
													ElseIf lastError.indexOf("Session") > -1 OR Request.QueryString("sessionEnded") = "1" Then
														Response.Write("Your session has ended, please log in again to view the Dashboard.")
													ElseIf Request.QueryString("processError") = "1" Then 
														Response.Write("There was an error processing your request, please try again.")
													ElseIf Request.QueryString("processError") = "2" Then 
														Response.Write("There was an error processing your request, it is possible you do not have sufficient access to view this page. <br /><br />Please contact support by clicking the link above if you need access.")
													ElseIf Request.QueryString("sessionEnded") = "1" Then 
														Response.Write("Your session has ended, please log in again to view the Dashboard.")
													ElseIf lastError <> "" Then 
														Response.Write("There was an error processing your request, please try again.")
													End If
													
												%>
											</td>
										</tr>
                                    </table>
                                </form>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</body>
</html>
