// NOTE: For every dashboard panel in the current dashboard tab, insert the custom action link structure, defined in the panel and having the class 'PanelCaptionActionLink', and move it to just before the Logi default cog link
window.onload = function ()
{
	var actionLink;
	var cogLink;
	var controlCell;
	
	//NOTE: For each Dashboard Panel
	$('div[id^="rdDashboardPanel"]').each(function(index) {

		actionLink = $(this).find('.PanelCaptionActionLink').parent();
		cogLink = $(this).find('img[id^="rdPanelSettingsCog_"]').parent();
		
		//NOTE: If Logi element structure tagged with (non-existent) "PanelCaptionActionLink" CSS class found
		if (actionLink.length > 0)
		{
			//NOTE: If Logi Dashboard Panel settings cog link found (not present when "Dashboard Adjustable" attribute set to False)
			if (cogLink.length > 0)
			{
				//NOTE: Move Logi action menu element structure to the dashboard panel caption bar just to the left of the Logi cog link
				actionLink.insertBefore(cogLink, actionLink);
			}
			else
			{
				controlCell = $(this).find('td#rdDashboardControl.rdDashboardControl');
				
				//NOTE: If the HTML <TABLE> cell (<TD>) containing the settings cog link found
				if (controlCell.length > 0)
				{
					//NOTE: Move Logi action menu element structure to the empty dashboard control table cell in the dashboard panel caption bar
					controlCell.append(actionLink);
				}
			}
		}
	});
}
