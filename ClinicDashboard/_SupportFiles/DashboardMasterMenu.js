$(document).ready(function ()
{
	// hide panel if clicking anywhere else
	$(document).click(function (e)
	{
		// clicking outside of filters button
		if (!$(e.target).closest('#divFilter').length)
		{
			// clicking outside of panel div
			if (!$(e.target).closest('#divFilterPanel').length)
			{
				if ($('#divFilterPanel').is(":visible"))
				{
					$('#divFilterPanel').hide();
				}
			}
		}

	}
	);

	// toggle show/hide on Filters button
	$('#divFilter').click(function ()
	{
		$('#divFilterPanel').toggle();
	}
	);
	// tie the filter panel to the filters button as a dropdown
	new Tether(
	{
		element : divFilterPanel,
		target : divFilter,
		attachment : 'top left',
		targetAttachment : 'bottom left'
	}
	);

}
);

var resetClick = function ()
{
	$("input:checkbox").prop("checked", true);

	/*reset all checkbox to checked minus select all checkbox */
	var cbSite = ($("#SiteSelectionList").find("input[type=checkbox]").length) - 1;
	$("#SiteSelectionList_handler span.rd-checkboxlist-caption").text(cbSite + " Sites Selected"); //change dropdown to number of selected checkbox

	/*same as above but minus "level 1" -- the grouping checkboxes*/
	var cbClinic = ($("#ClinicSelectionList").find("input[type=checkbox]").length) - (1 + ($("#ClinicSelectionList").find("[rdlevel=1]").length));
	$("#ClinicSelectionList_handler span.rd-checkboxlist-caption").text(cbClinic + " Clinics Selected");

	var cbProvider = ($("#ProviderSelectionList").find("input[type=checkbox]").length) - (1 + ($("#ProviderSelectionList").find("[rdlevel=1]").length));
	$("#ProviderSelectionList_handler span.rd-checkboxlist-caption").text(cbProvider + " Providers Selected");
}
