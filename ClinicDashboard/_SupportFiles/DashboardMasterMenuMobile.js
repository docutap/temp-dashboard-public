var resetClick = function () {
	$("input:checkbox").prop("checked", true);

	/*reset all checkbox to checked minus select all checkbox */
	var cbSite = ($("#SiteSelectionList").find("input[type=checkbox]").length) - 1;
	$("#SiteSelectionList_handler span.rd-checkboxlist-caption").text(cbSite + " Sites Selected"); //change dropdown to number of selected checkbox

	/*same as above but minus "level 1" -- the grouping checkboxes*/
	var cbClinic = ($("#ClinicSelectionList").find("input[type=checkbox]").length) - (1 + ($("#ClinicSelectionList").find("[rdlevel=1]").length));
	$("#ClinicSelectionList_handler span.rd-checkboxlist-caption").text(cbClinic + " Clinics Selected");

	var cbProvider = ($("#ProviderSelectionList").find("input[type=checkbox]").length) - (1 + ($("#ProviderSelectionList").find("[rdlevel=1]").length));
	$("#ProviderSelectionList_handler span.rd-checkboxlist-caption").text(cbProvider + " Providers Selected");
}

$(document).ready(function ()
{
	var isMobile = false;
	var rdMobile = getUrlParameter("rdMobile");
	
	//does rdMobile parameter exist
	if (rdMobile != undefined)
	{
		if (rdMobile.toLowerCase() === "true")
		{
			isMobile = true;
		}
		else
		{
			isMobile = false;
		}
	}

	if (isMobile == true)
	{
		//grab the <a href> link
		var actShowPanel = $("#actAddPanel").attr("onclick");

		/* append it to this button as an onclick event, as it will be moved
		the last part is a hack to remove the search panel 
		because Logi adds it back after every ajax request */
		$("#btnAddPanelMobile").attr("onclick", actShowPanel + "$('#divAddPanelsSearch').closest('td').remove();");
	}

	//toggle show/hide menu on hamburger menu click
	$("#HamburgerMenuImageMobile").click(function ()
	{

		/*show/hide the menu tree
		did not use .toggle() callback because css
		needs to be resolved first before function call */
		$("#divLeftMenuMobile").toggle(50);
		toggleScroll();
	}
	);
}
);

//close left menu if clicked outside of the menu
$(document).on("mouseup", function (e)
{
	var hamMenu = $("#HamburgerMenuImageMobile");
	var leftMenu = $("#divLeftMenuMobile");

	if (!hamMenu.is(e.target) //if target of click isn't hamburger div
		 && hamMenu.has(e.target).length === 0) // ... nor a descendent
	{
		if (!leftMenu.is(e.target) // if the target of the click isn't the leftMenu...
			 && leftMenu.has(e.target).length === 0) // ... nor a descendant of the leftMenu
		{
			//hide div and unlock scrolling
			leftMenu.hide();
			toggleScroll();
		}
	}
}
);


//hacky way to prevent scrolling
var toggleScroll = function toggleScroll()
{
	if ($("#divLeftMenuMobile").is(":hidden")) //visible
	{
		$("body").css('overflow', 'auto');
	}
	else //hidden
	{
		$("body").css('overflow', 'hidden');
	};
};