get-service  | where {$_.Name -eq "W3SVC"} | stop-service -Force

start-sleep -s 30

$serviceStatus = (get-service  | where {$_.Name -eq "W3SVC"}).status

if($serviceStatus -eq 'Stopped'){
	write-host "Service stopped"
	exit 0
} else {
	write-host "SErvice not stopped"
	exit 1
}